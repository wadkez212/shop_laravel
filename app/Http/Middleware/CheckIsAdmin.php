<?php

namespace App\Http\Middleware;

use Closure;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CheckIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        //if($user->isAdmin()) {
            //dd($user);
            //  $orders = Order::where('status', 1)
            //  ->get();
            //  return view('auth.orders.index', compact('orders'));
        //}

        return $next($request);
    }
}
