<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;   

class MainController extends Controller
{
    public function index () {
        $products = Product::get();
        return view('index', compact('products'));
    }
    public function categories () {
        $categories = Category::get();  //Получить все Категории
        return view('categories', compact('categories'));   //Передаем переменную через compact
    }
    public function category($code) {
        $category = Category::where('code', $code)->first();

        // Необходимо для вывода продуктов в card.php, и в category.php
        //но тк мы установли связь в меделе, то в category.php 
        //Переменная $products мы больше не используем, но используем связь в моделе Product и Category
        //$products = Product::where('category_id', $category->id)->get();
        //dd($code);
        return view('category', compact('category'));
    }
    public function product ($category, $product_code = null) {
        $product = Product::where('code', $product_code)->first();
        return view('product', ['product' => $product]);
    }
    // public function cart () {
    //     return view('cart');
    // }
    // public function order () {
    //     return view('order');
    // }
}
