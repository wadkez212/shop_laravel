<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    //В kernel.php прописаны все middleware, для auth Прописано если не зареган, то редирект на страницу login
    public function __construct()
    {
        //При авторизации попадаем на страницу home_lk (прописан путь home_lk в routeServiceProvider.php)
        //Поэтому этот маршрут делаем через Middleware, чтобы если не зарегистрированы редиректил на страницу login
        //и в home_lk было не попасть
        $this->middleware('auth');//->except('attendanceShowToStudent');
        //MiddleWare2 is_admin (Тут не нужен как и класс CheckIsAdmin и его алиас в Middleware тоже не нужен)
        //Нужен для проверки на администратора и например редиректа со страницы /orders если не админ
        $this->middleware('is_admin');
    }

    //Вывести заказы где status=1 и user_id = id зарегистрированного пользователя
    public function index() {
        $user = Auth::user();
        //Если не админ, то вывести заказы с id пользователя
        if(!$user->isAdmin()) { // Проверка на админа функция в фвйле User.php
            $orders = Order::where('status', 1)
            ->where('user_id', Auth::id())
            ->get();
            return view('auth.orders.index', compact('orders')); 
        } else {
            //Если админ то вывести все заказы
            $orders = Order::get();
            return view('auth.orders.index', compact('orders')); 
        }

    }
}
