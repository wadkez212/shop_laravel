<?php

namespace App\Http\Controllers;


use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function cart () {
        $order_id = session('order_id');
        if(!is_null($order_id)) {
            $order = Order::findOrFail($order_id);
        } else {
            $order = null;
        }
        return view('cart', compact('order'));
    }
    public function order () {
        $order_id = session('order_id');
        if(is_null($order_id)) {
            return redirect()->route('index');
        } else {
            $order = Order::find($order_id);
            //$order = Order::create();
            return view('order', compact('order'));
        }
    }

    //ОШИБКА ТК СОЗДАЕМ ЗАКАЗ ВО ВРЕМЯ ФОРМИРОВАНИЯ КОРЗИНЫ
    //ИЗ_ЗА ЭТОГО УСЛОВИЕ В CART.BLADE.PHP на проверку заказа если 0 то вывести корзина пуста, условие не работает 
    public function cartAdd ($product_id) 
    {
        //$order_id = null;
        $order_id = session('order_id');
        //Если товар в корзине, а заказа нет, то создаем запись в таблице Orders
        // (поле статус и другие поля по умолчанию 0)
        if(is_null($order_id)) {
            $order = Order::create();
            session(['order_id' => $order->id]);
        } else {
            $order = Order::find($order_id);
        }


        //Если он уже содержится в корзине заказа
        if($order->products->contains($product_id)) {
            //Найти этот товар и увеличить ему count
            //тк таблица order_products в моделе Order промежуточная, то используем вконце pivot
            $pivotRow = $order->products()->where('product_id', $product_id)->first()->pivot;
            //dd($pivotRow);
            $pivotRow->count++;
            $pivotRow->update();
        } else {
            //Запись в таблицу order_product через метод многие ко многи в моделе Order.php
            $order->products()->attach($product_id); 
        }

        //Заполняем полу user_id в таблице Orders
        if(Auth::check()) {
            $order->user_id = Auth::id();   //Если авторизован то поле user_id равно полю id из таблицы Users
            $order->save();         //Используем кода меняем данные в таблице
        }
        //Используем в cart.blade.php чтобы моги получить доступ к товару
        // и вывести какой товар добавили
        $product = Product::find($product_id);
        session()->flash('success', 'Товар '. $product->name . ' добавлен в корзину');
        //dump($order->products);
        //dump($order);
        //return view('cart', compact('order'));
        return back();//redirect()->route('cart');
        
    }
    public function cartRemove($product_id)
    {
        $order_id = session('order_id');
        if(is_null($order_id)) {
            //return view('cart', compact('order'));
            return redirect()->route('cart');
        }
        $order = Order::find($order_id);

        if($order->products->contains($product_id)) {
            $pivotRow = $order->products()->where('product_id', $product_id)->first()->pivot;
            if($pivotRow->count < 2) {
                $order->products()->detach($product_id); 
            } else {
                $pivotRow->count--;
                $pivotRow->update();
            }
        }


        //Используем в cart.blade.php чтобы моги получить доступ к товару
        //и вывести какой товар мы удалили
        $product = Product::find($product_id);
        session()->flash('error', 'Удален товар'. $product->name);
        //return view('cart', compact('order'));
        return redirect()->route('cart');
    }
    //Request - значит работаем с запросом в бд и будем через request вытаскивать данные
    public function cartConfirm(Request $request) {

        $order_id = session('order_id');
        if(is_null($order_id)) {
            return redirect()->route('index'); 
        } else {
            $order = Order::findOrFail($order_id);
            //Перенесли в Order.php function saveOrder()
            $success = $order->saveOrder($request->name, $request->phone);
            // $order->name = $request->name;
            // $order->phone = $request->phone;
            // $order->status = 1;
            // $order->save();
            if ($success) {
                session()->flash('success', 'Ваш заказ принят в обработку!');
            } else {
                session()->flash('error', 'Случилась ошибка');
            }

            
            return redirect()->route('index');
        }
    }
}
