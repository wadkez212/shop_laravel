<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //Заказ - Продукт - связь многие ко многим
    // withPivot - чтобы из проомежуточной таблицы order_product мы могли изменять count
    public function products() {
        return $this->belongsToMany(Product::class)->withPivot('count')->withTimestamps();
    }
    //use HasFactory;
    public function getFullPrice() {
        $sum = 0;
        foreach($this->products as $product) {
            $sum += $product->getPriceForCount();
        }
        return $sum;
    }
    public function saveOrder($name, $phone) {
        if($this->status == 0 ) {
            $this->name = $name;
            $this->phone = $phone;
            $this->status = 1;
            $this->save();
            session()->forget('order_id');
            return true;
        } else {
            return false;
        }

    }
}
