<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //Одна категория может содержать множество продуктов
    public function products() 
    {
        return $this->hasMany(Product::class);
    }
}
