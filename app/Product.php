<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    // public function getCategory()
    // {
    //     //$category = Category::where('id', $this->category_id)->first(); Не нужно
    //     return $category = Category::find($this->category_id);
    //     //dd($category); 
    // }
    //В одной категории может быть несколько продуктов (но функция должна вернуть одну категорию)
    public function category() 
    {
        return $this->belongsTo(Category::class);
    }
    //Изменить цену (Цена = кол-во * цена)
    //count находится в промежуточной таблице order_products поэтому получаем count через pivot
    public function getPriceForCount() {
        if(!is_null($this->pivot)) {
            return $this->pivot->count*$this->price;
        }
        return $this->price;
    }
}
