@extends('master')


@section('title', 'Корзина')

@section('content')
    <div class="starter-template">
        <h1>Корзина</h1>
        <p>Оформление заказа</p>
        {{-- Если есть заказ в корзине --}}
        @isset($order)
            {{-- @if (!is_null(session('order_id'))) --}}
            
                <div class="panel">
                    {{-- Указано в CartController.php cartConfirm() --}}
                    @if (session()->has('success'))
                        <p class="alert alert-success">{{session()->get('success')}}</p>
                    @endif
                    @if (session()->has('error'))
                        <p class="alert alert-warning">{{session()->get('error')}}</p>
                    @endif
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Кол-во</th>
                            <th>Цена</th>
                            <th>Стоимость</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($order->products as $product )
                            <tr>
                                <td>
                                    <a href="{{route('product', [$product->category->code, $product->code]) }}">
                                        <img height="56px" src="http://internet-shop.tmweb.ru/storage/products/iphone_x_silver.jpg">
                                        {{$product->name}}
                                    </a>
                                </td>
                                <td><span class="badge">{{$product->pivot->count}}</span>
                                    <div class="btn-group form-inline">
                                        <form action="{{route('cart_remove', $product)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-danger" href=""><span
                                                    class="glyphicon glyphicon-minus" aria-hidden="true">-</span></button>
                                        </form>
                                        <form action="{{route('cart_add', $product)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-success"
                                                    href=""><span
                                                    class="glyphicon glyphicon-plus" aria-hidden="true">+</span></button> 
                                        </form>                           
                                    </div>
                                </td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->getPriceForCount()}}</td>
                            </tr>
                            @endforeach
                            
                        
                            <tr>
                                <td colspan="3">Общая стоимость:</td>
                                <td>{{$order->getFullPrice()}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class="btn-group pull-right" role="group">
                        <a type="button" class="btn btn-success" href="{{route('order')}}">Оформить заказ</a>
                    </div>
                </div>
            {{-- @else --}}
                {{-- Если продуктов в корзине нет --}}
                {{-- В корзине нет продуктов --}}
            {{-- @endif --}}
        @endisset

        @if (!isset($order))
        {{-- Основной метод прописан в middleware BasketIsNotEmpty.php
            Это работает только для незарегистрированных пользователей
            когда отсутствует сессия order и мы не можем пробежаться по @foreach сверху--}}
            В корзине нет продуктов
            {{-- <script>window.location = "/";</script> --}}
            @php
                session()->flash('error', 'Ваша корзина пуста');
                header("Location: " . URL::to('/'), true, 302);
            // exit();
            @endphp
        @endif
    </div>
@endsection

