@extends('master')
@isset($category)
    @section('title', 'Категория '. $category->name)
@endisset


@section('content')
    <div class="starter-template">
        @if(session()->has('success'))
            <p class="alert alert-success">{{session()->get('success')}}</p>
        @endif
        @isset($category)
            <h1>{{$category->name}}</h1>
            <p>{{$category->description}}</p>
            <p>Всего {{$category->products->count()}} товара</p>
        @endisset
        @include('partitials.filters');
        <div class="row">
            {{-- ПРОКИДЫВЫЕТСЯ В CARD.PHP НО НА САМОМ ДЕЛЕ В CARD.PHP АДРЕС БЕРЕТСЯ НЕ ОТСЮДА А ИЗ MAINCONTROLLER function category($code)--}}
            {{-- @include('card', ['category' => $category]) --}}
            {{-- @foreach ($products as $product) --}}

            {{-- То же самое, но вместо верхнего foreach используем не в контрооллере, а в методе в Моделе где установили связь один ко многим (hasMany) 
                Теперь нам не нужно объяфвление $product в контроллере и можем его закомментить--}}
            @isset($category)
                @foreach ($category->products as $product)
                    @include('layouts.card', compact('product'))
                @endforeach
            @endisset
        </div>
        @include('partitials.pagination')
    </div>
@endsection

