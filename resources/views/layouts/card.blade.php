<div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <div class="labels">    
        </div>
        <img src="http://internet-shop.tmweb.ru/storage/products/iphone_x.jpg" alt="iPhone X 64GB">
        <div class="caption">
            @isset($product)
                <h3>{{$product->name}}</h3>
                <p>{{$product->price}} руб.</p>
            @endisset
            <p>
            <form action="{{route('cart_add', $product->id)}}" method="post">
                @csrf  
                <button type="submit" class="btn btn-primary" role="button">В корзину</button>
                {{-- ЧТобы не оборачивать в isset Написали запрос getCategory() в моделе Product.php --}}
                {{-- {{$product->getCategory()->name}} --}} 
                {{-- Тоже самое но уже через связи многие к одному и без @isset --}}
                {{$product->category->name}}
                
                {{-- @isset($category) --}}
                {{-- {{$category->code}} --}}
                    <a href="{{route('product',[$product->category->code, $product->code])}}"
                        class="btn btn-default"
                        role="button">Подробнее</a>  
                {{-- @endisset --}}
                
                        
            </form>
            </p>
        </div>
    </div>
</div>