{{-- content - Используется в мастер шаблоне через @yield --}}
{{-- То что не в мастер шаблоне подключается через @include --}}
@extends('master')      
{{-- Выводит туда контент из переменной @yield('title') --}}
@section('title', 'Главная')
{{-- ЗАВИСИТ ОТ ШАБЛОНА MASTER, и туда вывожит предполагаемый контент через @yield('content') --}}
@section('content')
    <div class="starter-template">
        {{-- Указано в CartController.php cartConfirm() --}}
        @if (session()->has('success'))
            <p class="alert alert-success">{{session()->get('success')}}</p>
        @endif

        @if (session()->has('error'))
            <p class="alert alert-warning">{{session()->get('error')}}</p>
        @endif
        <h1>Все товары</h1>

        @include('partitials.filters');

        <div class="row">
            @foreach ($products as $product)
                @include('layouts.card')
            @endforeach
        </div>
        @include('partitials.pagination')
    </div>
@endsection

