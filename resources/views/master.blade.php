<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Интернет Магазин</title>

        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
        <link href="/css/starter-template.css" rel="stylesheet">
        <script src="/js/main.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ route('index') }}">Интернет Магазин</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{ route('index') }}">Все товары</a></li>
                        <li ><a href="{{ route('categories') }}">Категории</a></li>
                        <li ><a href="{{ route('cart') }}">В корзину</a></li>
                        <li><a href="{{ route('index') }}">Сбросить проект в начальное состояние</a></li>
                        <li><a href="/locale/en">en</a></li>
    
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">₽<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/currency/RUB">₽</a></li>
                                <li><a href="/currency/USD">$</a></li>
                                <li><a href="/currency/EUR">€</a></li>
                            </ul>
                        </li>
                    </ul>
    
                    <ul class="nav navbar-nav navbar-right">
                        @guest
                            <li><a href="{{route('login')}}">Войти</a></li>
                            <li><a href="{{route('register')}}">Зарегистрироваться</a></li>
                        @endguest
                        
                        @auth
                            <li><a href="{{route('home')}}">Панель администратора</a></li>
                            <li><a href="{{route('logout')}}">Выйти</a></li>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>
       
        <div class="container">
            @yield('title')            
            @yield('content')
        </div>   
    </body>
</html>
    