<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

//use Illuminate\Routing\Route;

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//Вывести маршруты для авторизации
Auth::routes();
Route::get('/orders', [App\Http\Controllers\OrderController::class, 'index'])->name('home');

//Login работает через масетр шаблон со ссылкой на страницу /register
//Можно посмотреть через php artisan route:list МАРШРУТЫ register, login, logout уже есть по уумолчанию
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

 Route::group([
     //В kernel.php Прописано если не зареган, то редирект на страницу login
    //Все middleware прописаны в OrderController.php для редиректа с /orders если не зареган и тд, тк связаны с лк 
    //namespace нужен чтобы опознать файл в папке /Controllers/Admin/OrderControlleer.php
     'namespace' => 'Admin',
     ], function() {
    Route::get('/orders', 'OrderController@index')->name('home');
});


Route::get('/', "MainController@index")->name('index');

Route::post('/cart/add/{id}', 'CartController@cartAdd')->name('cart_add');

//Редирект с корзины на главную страницу в middleware BasketIsNotEmpty.php
//Там же прописываем оповещение через slash в BasketIsNotEmpty.php в сесиию которую выводим в index.php в условии  @if (session()->has('error'))
Route::group([
    'middleware' => 'basket_not_empty',
], function() {
    Route::get('/cart', "CartController@cart")->name('cart');
    Route::get('/cart/order', "CartController@order")->name('order');
    Route::post('/cart/place', "CartController@cartConfirm")->name('cart_confirm');
    Route::post('/cart/remove/{id}', 'CartController@cartRemove')->name('cart_remove');
});




Route::get('/categories', "MainController@categories")->name('categories');
Route::get('/{category}', "MainController@category")->name('category');
Route::get('/{category}/{product?}',  "MainController@product")->name('product');







